<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class trajectplanner extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'role' => 'admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'role' => 'student',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        DB::table('users')->insert([
            [
                'name' => 'Atal',
                'lastname' => 'Abdul Khalil',
                'email' => 'atal.abd@gmail.com',
                'password' => Hash::make('Azerty123'),
                'role_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'aima',
                'lastname' => 'akinlade',
                'email' => 'aima.akinlade@gmail.com',
                'password' => Hash::make('Azerty123'),
                'role_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        DB::table('subjects')->insert([
            [
                'name' => 'Boekhouding',
                'code' => 'QSD133',
                'credit' => 6,
                'phase' => 3,
                'semester' => '1',
                'period' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Carrosserie',
                'code' => 'khG452',
                'credit' => 6,
                'phase' => 3,
                'semester' => '1',
                'period' => '2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Chemie',
                'code' => 'lbG120',
                'credit' => 3,
                'phase' => 3,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Cultuurbeschouwing',
                'code' => 'dfG021',
                'credit' => 3,
                'phase' => 3,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Cultuurwetenschappen',
                'code' => 'cdB021',
                'credit' => 12,
                'phase' => 3,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Dactylografie',
                'code' => 'kz103R',
                'credit' => 12,
                'phase' => 3,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Economie',
                'code' => 'jaH521',
                'credit' => 6,
                'phase' => 3,
                'semester' => '2',
                'period' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Eigen cultuur en religie',
                'code' => 'Jni021',
                'credit' => 6,
                'phase' => 3,
                'semester' => '2',
                'period' => '2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Fysica',
                'code' => 'Jcj021',
                'credit' => 6,
                'phase' => 3,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Instrument',
                'code' => 'XXj021',
                'credit' => 6,
                'phase' => 3,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'C# Object Oriented Programming',
                'code' => 'QSD150',
                'credit' => 6,
                'phase' => 2,
                'semester' => '1',
                'period' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'C# Programming Techniques',
                'code' => 'JHG452',
                'credit' => 6,
                'phase' => 2,
                'semester' => '1',
                'period' => '2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Ethiek',
                'code' => 'HJG120',
                'credit' => 3,
                'phase' => 2,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Professionele Communicatie',
                'code' => 'JHG021',
                'credit' => 3,
                'phase' => 2,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Web & Mobile Server-side',
                'code' => 'JHB021',
                'credit' => 12,
                'phase' => 2,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Web & Mobile Full-stack',
                'code' => 'OG103R',
                'credit' => 12,
                'phase' => 2,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Algorithm & Data',
                'code' => 'HGH521',
                'credit' => 6,
                'phase' => 2,
                'semester' => '2',
                'period' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Data Science',
                'code' => 'JmG021',
                'credit' => 6,
                'phase' => 2,
                'semester' => '2',
                'period' => '2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Project & Wetenschapelijk Rapporteren',
                'code' => 'JaG021',
                'credit' => 6,
                'phase' => 2,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Handel',
                'code' => 'QWG021',
                'credit' => 6,
                'phase' => 2,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Nederlands',
                'code' => 'spD150',
                'credit' => 6,
                'phase' => 1,
                'semester' => '1',
                'period' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Engels',
                'code' => 'JVG452',
                'credit' => 6,
                'phase' => 1,
                'semester' => '1',
                'period' => '2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Rekenen en wiskunde',
                'code' => 'asG120',
                'credit' => 3,
                'phase' => 1,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Godsdienst',
                'code' => 'JoG021',
                'credit' => 3,
                'phase' => 1,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Frans',
                'code' => 'JkB021',
                'credit' => 12,
                'phase' => 1,
                'semester' => '1',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Duits',
                'code' => 'OG453R',
                'credit' => 12,
                'phase' => 1,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Autotechniek',
                'code' => 'HGl521',
                'credit' => 6,
                'phase' => 1,
                'semester' => '2',
                'period' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Biochemie',
                'code' => 'Jml021',
                'credit' => 6,
                'phase' => 1,
                'semester' => '2',
                'period' => '2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Biologie',
                'code' => 'JHb021',
                'credit' => 6,
                'phase' => 1,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Grieks',
                'code' => 'Jlo021',
                'credit' => 6,
                'phase' => 1,
                'semester' => '2',
                'period' => '1&2',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);

        DB::table('trajectories')->insert([
            [
                'favorite' => 0,
                'shared' => 1,
                'accepted' => 0,
                'user_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'favorite' => 0,
                'shared' => 0,
                'accepted' => 0,
                'user_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        DB::table('subject_trajectory')->insert([
            [
                'subject_id' => 1,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 2,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 3,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 4,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 5,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 6,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 7,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 8,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 9,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 1,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 2,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 3,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 4,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 5,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 6,
                'trajectory_id' => 2
            ],
        ]);
    }
}
