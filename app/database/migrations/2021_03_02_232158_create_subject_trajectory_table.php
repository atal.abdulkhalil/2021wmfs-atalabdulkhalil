<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectTrajectoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_trajectory', function (Blueprint $table) {
            /*$table->id();
            $table->timestamps();*/

            $table->unsignedBigInteger('subject_id')->index();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->unsignedBigInteger('trajectory_id')->index();
            $table->foreign('trajectory_id')->references('id')->on('trajectories')->onDelete('cascade');
            $table->primary(['subject_id', 'trajectory_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_trajectory');
    }
}
