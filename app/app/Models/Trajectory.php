<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trajectory extends Model
{
    use HasFactory;

    protected $fillable = ['favorite', 'shared', 'accepted', 'user_id'];
    protected $hidden = ['user_id'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i',
        'updated_at'=> 'datetime:Y-m-d H:i',
    ];

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFakeUser($query)
    {
        return $query->where('user_id', 2);
    }
}
