<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code', 'credit', 'phase', 'semester', 'period'];
    protected $hidden = ['pivot', 'updated_at'];

    public function trajectories()
    {
        return $this->belongsToMany(Trajectory::class);
    }

    public function getNameAttribute($value) {
        return Str::title($value);
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = strtolower($value);
    }
}
