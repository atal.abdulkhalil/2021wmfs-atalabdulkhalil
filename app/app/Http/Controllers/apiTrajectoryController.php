<?php

namespace App\Http\Controllers;

use App\Models\Trajectory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class apiTrajectoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/api/trajectories",
     *      operationId="getTrajectoriesList",
     *      tags={"Trajectories"},
     *      summary="Get list of trajectories",
     *      description="Returns list of trajectories",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function index()
    {
        return ['data' => Trajectory::with('subjects')
//            ->fakeUser()
            ->where('user_id', Auth::user()->id)
            ->get()];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/trajectories",
     *      operationId="storeTrajectory",
     *      tags={"Trajectories"},
     *      summary="Store new trajectory",
     *      description="Returns 201 status response",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="favorite", type="integer", example=0),
     *              @OA\Property(property="shared", type="integer", example=1),
     *              @OA\Property(property="subjects", type="integer", example={1,7}),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function store(Request $request)
    {
        $request->validate([
//            'favorite' => ['required', Rule::in([0, 1])],
//            'shared' => ['required', Rule::in([0, 1])],
            'subjects' => 'required|exists:subjects,id'
        ]);
        $trajectory = new Trajectory($request->all());
        $trajectory->user_id = Auth::user()->id;
        $trajectory->save();
        Trajectory::findorfail($trajectory->id)->subjects()->sync($request->subjects);
        return response()->json(['message' => 'The trajectory has been created'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/api/trajectories/{id}",
     *      operationId="getTrajectoryById",
     *      tags={"Trajectories"},
     *      summary="Get a trajectory by id",
     *      description="Returns a trajectory",
     *      @OA\Parameter(
     *          name="id",
     *          description="Trajectory id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function show($id)
    {
        return ['data' => Trajectory::with('subjects')
//            ->fakeUser()
            ->where('user_id', Auth::user()->id)
            ->findOrFail($id)];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Put(
     *      path="/api/trajectories/{id}",
     *      operationId="updateTrajectory",
     *      tags={"Trajectories"},
     *      summary="Update existing trajectory",
     *      description="returns succes message",
     *      @OA\Parameter(
     *          name="id",
     *          description="Trajectory id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="favorite", type="integer", example=0),
     *              @OA\Property(property="shared", type="integer", example=1),
     *              @OA\Property(property="subjects", type="integer", example={1,7}),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function update(Request $request, $id)
    {
        $request->validate([
//            'favorite' => ['required', Rule::in([0, 1])],
//            'shared' => ['required', Rule::in([0, 1])],
//            'subjects' => 'required|exists:subjects,id'
            'favorite' => [Rule::in([0, 1])],
            'shared' => [Rule::in([0, 1])],
            'subjects' => 'exists:subjects,id'
        ]);
        $trajectory = Trajectory::Where('user_id', Auth::user()->id)->findOrFail($id);
        if ($request->subjects){
            Trajectory::findorfail($trajectory->id)->subjects()->sync($request->subjects);
        }
        $trajectory->update($request->all());

        return response()->json(['message' => 'The trajectory has been updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Delete(
     *      path="/api/trajectories/{id}",
     *      operationId="deleteTrajectory",
     *      tags={"Trajectories"},
     *      summary="Delete existing trajectory",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Trajectory id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function destroy($id)
    {
        $trajectory = Trajectory::Where('user_id', Auth::user()->id)->findOrFail($id);
        $trajectory->delete();

        return response()->json(null, 204);
    }
}
