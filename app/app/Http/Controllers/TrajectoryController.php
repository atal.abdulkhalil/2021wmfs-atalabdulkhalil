<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\Trajectory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrajectoryController extends Controller
{
    public function overview(){
        if (Auth::user()->role_id === 1){
            $trajectories = Trajectory::where('shared', 1)
                ->orderBy('created_at', 'desc')
                ->get();
        }

        else {
            $trajectories = Trajectory::where('user_id', Auth::user()->id)
                ->orderBy('created_at', 'desc')
                ->get();
        }
        return view('trajectories', ['trajectories' => $trajectories]);
    }

    public function trajectoryDetails(Trajectory $trajectory, Request $request){
        $subjects = Subject::whereHas('trajectories', function ($q) use ($trajectory) {
            $q->where('id', $trajectory->id);
        })->get();
        $trajectory = Trajectory::findOrFail($trajectory->id);
/*
        if ($request->user()->cannot('view', $trajectory)) {
            abort(403);
        }*/
        return view('trajectory-detail', ['subjects' => $subjects, 'trajectory' => $trajectory]);
    }

    public function showTrajectoryDelete(Trajectory $trajectory){
        return view('trajectory-actions', ['trajectory' => $trajectory, 'accept' => false, 'edit' => false]);
    }

    public function trajectoryDelete(Trajectory $trajectory){

        $trajectory = Trajectory::findOrFail($trajectory->id);
        $trajectory->delete();
//        $flight->name = 'Paris to London';
//        $flight->save();
        return redirect('/dashboard/trajectories');
    }

    public function showTrajectoryAccept(Trajectory $trajectory){
        return view('trajectory-actions', ['trajectory' => $trajectory, 'accept' => true]);
    }

    public function trajectoryAccept(Trajectory $trajectory){

        $trajectory = Trajectory::findOrFail($trajectory->id);
        $trajectory->accepted = 1;
        $trajectory->save();
        return redirect('/dashboard/trajectories/'. $trajectory->id . '/details');
    }

    public function showTrajectoryEdit(Trajectory $trajectory){
        return view('trajectory-actions', ['trajectory' => $trajectory, 'accept' => false, 'edit' => true]);
    }

    public function trajectoryEdit(Trajectory $trajectory){

        $trajectory = Trajectory::findOrFail($trajectory->id);
        $trajectory->accepted = 0;
        $trajectory->save();
        return redirect('/dashboard/trajectories/'. $trajectory->id . '/details');
    }
}
