<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class apiSubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/api/subjects",
     *      operationId="getSubjectsList",
     *      tags={"Subjects"},
     *      summary="Get list of Subjects",
     *      description="Returns list of subjects",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function index()
    {
        return ['data' => Subject::all()];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     *      path="/api/subjects",
     *      operationId="storeSubject",
     *      tags={"Subjects"},
     *      summary="Store new subject",
     *      description="Returns 201 status response",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="name", type="string", example="java"),
     *              @OA\Property(property="code", type="string", example="45fgVF"),
     *              @OA\Property(property="credit", type="integer", example=6),
     *              @OA\Property(property="phase", type="integer", example=2),
     *              @OA\Property(property="semester", type="string", example="1"),
     *              @OA\Property(property="period", type="string", example="1")
     *          ),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      ),
     *     @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      )
     * )
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:40',
            'code' => 'required|unique:subjects|size:6',
            'credit' => 'required|numeric|min:1|max:24',
            'phase' => ['required', Rule::in([1, 2, 3])],
            'semester' => ['required', Rule::in(['1', '2', '1&2'])],
            'period' => ['required', Rule::in(['1', '2', '1&2'])]
        ]);
        $subject = new Subject($request->all());
        $subject->save();
        return response()->json(['message' => 'The subject has been created'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/api/subjects/{id}",
     *      operationId="getSubjectById",
     *      tags={"Subjects"},
     *      summary="Get a subject by id",
     *      description="Returns a subject",
     *      @OA\Parameter(
     *          name="id",
     *          description="Subject id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function show($id)
    {

        return ['data' => Subject::findOrFail($id)];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Put(
     *      path="/api/subjects/{id}",
     *      operationId="updateSubject",
     *      tags={"Subjects"},
     *      summary="Update existing subject",
     *      description="returns succes message",
     *      @OA\Parameter(
     *          name="id",
     *          description="Subject id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *
     *              @OA\Property(property="name", type="string", example="java"),
     *              @OA\Property(property="code", type="string", example="45fgVF"),
     *              @OA\Property(property="credit", type="integer", example=6),
     *              @OA\Property(property="phase", type="integer", example=2),
     *              @OA\Property(property="semester", type="string", example="1"),
     *              @OA\Property(property="period", type="string", example="1")
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:40',
            'code' => 'required|exists:subjects|size:6',
            'credit' => 'required|numeric|min:1|max:24',
            'phase' => ['required', Rule::in([1, 2, 3])],
            'semester' => ['required', Rule::in(['1', '2', '1&2'])],
            'period' => ['required', Rule::in(['1', '2', '1&2'])]
        ]);
        $subject = Subject::findOrFail($id);
        $subject->update($request->all());

        //return response()->json($subject, 201);
        return response()->json(['message' => 'The subject has been edited'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Delete(
     *      path="/api/subjects/{id}",
     *      operationId="deleteSubject",
     *      tags={"Subjects"},
     *      summary="Delete existing Subject",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Subject id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      ),
     *     @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *      )
     * )
     */
    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();

        return response()->json(null, 204);
    }
}
