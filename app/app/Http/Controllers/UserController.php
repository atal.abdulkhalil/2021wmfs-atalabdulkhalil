<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function overview(){
        Gate::authorize('edit-or-delete-subject');
        $students = User::Where('role_id', '2')->get();
        return view('students', ['students' => $students]);
    }

    public function showDeleteStudent(User $student){
        return view('delete-page', ['item' => $student, 'student' => true]);
    }

    public function deleteStudent(User $student){

        $student = User::findOrFail($student->id);
        $student->delete();
        return redirect('/dashboard/students');
    }
}
