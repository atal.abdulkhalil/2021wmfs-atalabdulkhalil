<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class SubjectController extends Controller
{
  //TODO zoeken aanpassen
    public function overview(Request $request){
        $phase = $request->phase;
        $semester = $request->semester;
        $term = $request->name;
        if ($request) {
            $subjects = Subject::
            when($term, function ($q, $term){
                return $q->where('name', 'like', '%' . $term . '%');
            })
                ->when($semester, function ($q, $semester){
                    return $q->where('semester', $semester);
                })
                ->when($phase, function ($q, $phase){
                    return $q->where('phase', $phase);
                })
                ->get();
//                ->paginate(5);

            return view('subjects', ['subjects' => $subjects, 'semester' => $semester, 'phase' => $phase, 'name' => $term]);
        }

//        if ($request->name && $request->phase && $request->semester) {
//            $phase = $request->phase;
//            $semester = $request->semester;
//            $term = $request->name;
//            $subjects = Subject::where ('name', 'like', '%' . $term . '%')
//                ->where ('phase', $phase)
//                ->where ('semester', $semester)
//                ->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'semester' => $semester, 'phase' => $phase, 'name' => $term]);
//        }
//
//        elseif ($request->name && $request->phase) {
//            $phase = $request->phase;
//            $term = $request->name;
//            $subjects = Subject::where ('name', 'like', '%' . $term . '%')
//                ->where ('phase', $phase)
//                ->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'phase' => $phase, 'name' => $term]);
//        }
//
//        elseif ($request->name && $request->semester) {
//            $semester = $request->semester;
//            $term = $request->name;
//            $subjects = Subject::where ('name', 'like', '%' . $term . '%')
//                ->where ('semester', $semester)
//                ->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'semester' => $semester, 'name' => $term]);
//        }
//
//        if ($request->phase && $request->semester) {
//            $phase = $request->phase;
//            $semester = $request->semester;
//            $subjects = Subject::where('phase', $phase)
//                ->where ('semester', $semester)
//                ->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'semester' => $semester, 'phase' => $phase]);
//        }
//
//        elseif ($request->name) {
//            $term = $request->name;
//            $subjects = Subject::where ('name', 'like', '%' . $term . '%')->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'name' => $term]);
//        }
//
//        elseif ($request->phase) {
//            $phase = $request->phase;
//            $subjects = Subject::where ('phase', $phase)->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'phase' => $phase]);
//        }
//        elseif ($request->semester) {
//            $semester = $request->semester;
//            $subjects = Subject::where ('semester', $semester)->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'semester' => $semester]);
//        }
//        elseif ($request->name && $request->phase) {
//            $phase = $request->phase;
//            $term = $request->name;
//            $subjects = Subject::where ('name', 'like', '%' . $term . '%')
//                ->where ('phase', $phase)
//                ->paginate(5);
//            return view('subjects', ['subjects' => $subjects, 'phase' => $phase, 'name' => $term]);
//        }

        $subjects = Subject::paginate(5);
        return view('subjects', ['subjects' => $subjects]);
    }


    public function subjectDetails($id){
        $subject = Subject::findOrFail($id);
        return view('subject-detail', ['subject' => $subject]);
    }

    public function showAddSubject(){
        //Gate::authorize('edit-or-delete-subject');
        return view('subject-add');
    }

    public function addSubject(Request $request){
        //Gate::authorize('edit-or-delete-subject');
        $request->validate([
            'name' => 'required|max:40',
            'code' => 'required|unique:subjects|size:6',
            'credit' => 'required|numeric|min:1|max:24',
            'phase' => ['required', Rule::in([1, 2, 3])],
            'semester' => ['required', Rule::in(['1', '2', '1&2'])],
            'period' => ['required', Rule::in(['1', '2', '1&2'])]
        ]);

        Subject::create($request->all());
        return redirect('/dashboard/subjects');
    }

    public function showEditSubject($id){
        Gate::authorize('edit-or-delete-subject');
        $subject = Subject::findOrFail($id);
        return view('subject-edit', ['subject' => $subject]);
    }

    public function editSubject(Request $request){
        Gate::authorize('edit-or-delete-subject');
        $request->validate([
            'name' => 'required|max:40',
            'code' => 'required|exists:subjects|size:6',
            'credit' => 'required|numeric|min:1|max:12',
            'phase' => ['required', Rule::in([1, 2, 3])],
            'semester' => ['required', Rule::in(['1', '2', '1&2'])],
            'period' => ['required', Rule::in(['1', '2', '1&2'])],
        ]);

        $subject = Subject::findOrFail($request->id);
        $subject->update($request->all());

        return redirect('/dashboard/subjects/'. $request->id . '/details');
    }

    public function showDeleteSubject(Subject $subject){
        return view('delete-page', ['item' => $subject, 'student' => false]);
    }

    public function deleteSubject(Subject $subject){

        $subject = Subject::findOrFail($subject->id);
        $subject->delete();
        return redirect('/dashboard/subjects');
    }
}
