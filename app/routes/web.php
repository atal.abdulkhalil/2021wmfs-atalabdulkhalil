<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');*/
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return redirect('./dashboard');
    });
    Route::get('/dashboard', [\App\Http\Controllers\DashboardController::class, 'overview']);
    Route::get('/dashboard/trajectories', [\App\Http\Controllers\TrajectoryController::class, 'overview']);
    Route::get('/dashboard/trajectories/{trajectory}/details', [\App\Http\Controllers\TrajectoryController::class, 'trajectoryDetails'])->where(['id' => '[0-9]+'])->middleware('can:view,trajectory');
    Route::get('/dashboard/trajectories/{trajectory}/delete', [\App\Http\Controllers\TrajectoryController::class, 'showTrajectoryDelete'])->where(['id' => '[0-9]+'])->middleware('can:edit-or-delete-subject');
    Route::post('/dashboard/trajectories/{trajectory}/delete', [\App\Http\Controllers\TrajectoryController::class, 'trajectoryDelete'])->middleware('can:view,trajectory');
    Route::get('/dashboard/trajectories/{trajectory}/accept', [\App\Http\Controllers\TrajectoryController::class, 'showTrajectoryAccept'])->where(['id' => '[0-9]+'])->middleware('can:edit-or-delete-subject');
    Route::post('/dashboard/trajectories/{trajectory}/accept', [\App\Http\Controllers\TrajectoryController::class, 'trajectoryAccept'])->middleware('can:edit-or-delete-subject');
    Route::get('/dashboard/trajectories/{trajectory}/edit', [\App\Http\Controllers\TrajectoryController::class, 'showTrajectoryEdit'])->where(['id' => '[0-9]+'])->middleware('can:edit-or-delete-subject');
    Route::post('/dashboard/trajectories/{trajectory}/edit', [\App\Http\Controllers\TrajectoryController::class, 'trajectoryEdit'])->middleware('can:edit-or-delete-subject');

    Route::get('/dashboard/students', [\App\Http\Controllers\UserController::class, 'overview']);
    Route::get('/dashboard/students/{student}/delete', [\App\Http\Controllers\UserController::class, 'showDeleteStudent'])->middleware('can:edit-or-delete-subject');
    Route::post('/dashboard/students/{student}/delete', [\App\Http\Controllers\UserController::class, 'deleteStudent'])->middleware('can:edit-or-delete-subject');

    Route::get('/dashboard/subjects', [\App\Http\Controllers\SubjectController::class, 'overview']);
    Route::post('/dashboard/subjects', [\App\Http\Controllers\SubjectController::class, 'overview']);
    Route::get('/dashboard/subjects/{id}/details', [\App\Http\Controllers\SubjectController::class, 'subjectDetails'])->where(['id' => '[0-9]+']);
    Route::get('/dashboard/subjects/add', [\App\Http\Controllers\SubjectController::class, 'showAddSubject'])->middleware('can:edit-or-delete-subject');
    Route::post('/dashboard/subjects/add', [\App\Http\Controllers\SubjectController::class, 'addSubject'])->middleware('can:edit-or-delete-subject');
    Route::get('/dashboard/subjects/{id}/edit', [\App\Http\Controllers\SubjectController::class, 'showEditSubject']);
    Route::post('/dashboard/subjects/{id}/edit', [\App\Http\Controllers\SubjectController::class, 'editSubject']);
    Route::get('/dashboard/subjects/{subject}/delete', [\App\Http\Controllers\SubjectController::class, 'showDeleteSubject'])->middleware('can:edit-or-delete-subject');
    Route::post('/dashboard/subjects/{subject}/delete', [\App\Http\Controllers\SubjectController::class, 'deleteSubject'])->middleware('can:edit-or-delete-subject');
});


Route::post('/sanctum/login', function (Request $request) {
    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials)) {
        // $request->session()->regenerate();
        return response($request->user(), 200);
    }
    return response(['message' => 'The provided credentials do not match our records.'], 422);

});

Route::post('/sanctum/logout', function (Request $request) {
    Auth::guard('web')->logout();
    $request->session()->invalidate();
    return response(['message' => 'The user has been logged out successfully'], 200);
});
require __DIR__.'/auth.php';

