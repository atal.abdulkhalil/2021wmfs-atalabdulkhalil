@extends('layout')

@section('title', 'Vakken')

@section('content')

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-8 offset-md-2">

                    @include('errors')
                    <div class="card">
                        <div class="card-header">
                            <strong>Vak bewerken</strong>
                        </div>
                        <div class="card-body card-block">
                            <form action="{{ url('/dashboard/subjects/' . $subject->id . '/edit') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                @csrf
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class=" form-control-label">Naam</label></div>
                                    <div class="col-12 col-md-9"><input type="text" id="name" name="name" placeholder="Vak naam" class="form-control" value="{{ old('name', $subject->name) }}"></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="code" class=" form-control-label">Code</label></div>
                                    <div class="col-12 col-md-9"><input type="text" id="code" name="code" placeholder="Vak code" class="form-control" value="{{ old('code', $subject->code) }}"></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="credit" class=" form-control-label">Aantal studiepunten</label></div>
                                    <div class="col-12 col-md-9"><input type="number" id="credit" name="credit" placeholder="Studiepunten voor dit vak" class="form-control" value="{{ old('credit', $subject->credit) }}"></div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Fase</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="radio">
                                                <label for="phase1" class="form-check-label ">
                                                    <input type="radio" id="phase1" name="phase" value="1" @if(old('phase', $subject->phase) == '1') checked="checked" @endif class="form-check-input">Fase 1
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label for="phase2" class="form-check-label ">
                                                    <input type="radio" id="phase2" name="phase" value="2" @if(old('phase', $subject->phase) == '2') checked="checked" @endif class="form-check-input">Fase 2
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label for="phase3" class="form-check-label ">
                                                    <input type="radio" id="phase3" name="phase" value="3" @if(old('phase', $subject->phase) == '3') checked="checked" @endif class="form-check-input">Fase 3
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="semester" class=" form-control-label">Semester</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="semester" id="semester" class="form-control">
                                            <option value="0">kies een semester</option>
                                            <option value="1" @if(old('semester', $subject->semester) === '1') selected="selected" @endif>semester 1</option>
                                            <option value="2" @if(old('semester', $subject->semester) === '2') selected="selected" @endif>semester 2</option>
                                            <option value="1&2" @if(old('semester', $subject->semester) === '1&2') selected="selected" @endif>semester 1 en 2</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="period" class=" form-control-label">Periode</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="period" id="period" class="form-control">
                                            <option value="0">Kies een periode</option>
                                            <option value="1" @if(old('period', $subject->period) === '1') selected="selected" @endif>Periode 1</option>
                                            <option value="2" @if(old('period', $subject->period) === '2') selected="selected" @endif>Periode 2</option>
                                            <option value="1&2" @if(old('period', $subject->period) === '1&2') selected="selected" @endif>Periode 1 en 2</option>
                                        </select>
                                    </div>
                                </div>

                                <input name="moduleAction" type="hidden" value="update"/>
                                <input class="btn btn-outline-success" type="submit" value="bewerken">
                            </form>
                        </div>
                    </div>

                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
    </div>


@endsection
