@extends('layout')

@section('title', 'Trajecten')

@section('content')

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">{{$trajectory->user->name}} {{$trajectory->user->lastname}}</strong>
                        </div>
                        @if($accept)
                            <div class="card-body">
                                <p>Wil je zeker de traject van <strong class="card-title">{{$trajectory->user->name}} {{$trajectory->user->lastname}}</strong>
                                Gemaakt op (<strong class="card-title">{{ date('d M y - H\ui', strtotime($trajectory->created_at)) }}</strong>) accepteren?</p>
                            </div>
                            <div class="card-body text-center">

                                <form method="post" action="{{ url('/dashboard/trajectories/' . $trajectory->id . '/accept') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-success"><i class="fa fa-check-square-o"></i>&nbsp ja</button>
                                </form>

                                <a class="btn btn-outline-secondary mt-3" href="{{ url('/dashboard/trajectories/'. $trajectory->id . '/details') }}" role="button"><i class="fa fa-hand-o-left"></i>&nbsp terug gaan</a>

                            </div>

                            @elseif($edit)
                                <div class="card-body">
                                    <p>Wil je zeker voor de traject van <strong class="card-title">{{$trajectory->user->name}} {{$trajectory->user->lastname}}</strong>
                                        Gemaakt op (<strong class="card-title">{{ date('d M y - H\ui', strtotime($trajectory->created_at)) }}</strong>) de accepteren ongedaan maken?</p>
                                </div>
                                <div class="card-body text-center">

                                    <form method="post" action="{{ url('/dashboard/trajectories/' . $trajectory->id . '/edit') }}">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-warning"><i class="fa fa-edit (alias)"></i>&nbsp ja</button>
                                    </form>

                                    <a class="btn btn-outline-secondary mt-3" href="{{ url('/dashboard/trajectories/'. $trajectory->id . '/details') }}" role="button"><i class="fa fa-hand-o-left"></i>&nbsp terug gaan</a>

                                </div>

                        @else
                            <div class="card-body">
                                <p>Wil je zeker de traject van <strong class="card-title">{{$trajectory->user->name}} {{$trajectory->user->lastname}}</strong>
                                    Gemaakt op (<strong class="card-title">{{ date('d M y - H\ui', strtotime($trajectory->created_at)) }}</strong>) verwijderen?</p>
                            </div>
                            <div class="card-body text-center">

                                <form method="post" action="{{ url('/dashboard/trajectories/' . $trajectory->id . '/delete') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash-o"></i>&nbsp ja</button>
                                </form>

                                <a class="btn btn-outline-secondary mt-3" href="{{ url('/dashboard/trajectories/'. $trajectory->id . '/details') }}" role="button"><i class="fa fa-hand-o-left"></i>&nbsp terug gaan</a>

                            </div>
                        @endif

                    </div>

                </div>

            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


@endsection
