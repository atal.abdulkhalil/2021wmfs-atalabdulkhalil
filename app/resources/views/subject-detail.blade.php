@extends('layout')

@section('title', 'Vakken')

@section('content')

    <div class="content">
        <div class="animated fadeIn">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Vak overzicht</strong>
                    </div>
                    <div class="card-body" id="custom-detail">
                        <div class="typo-headers">
                            <div class="row">
                                <div class="col-md-12 col-sm-12  ">
                                    <div class="x_panel">
                                        <h2 class="pb-2 display-5 text-center">{{$subject->name}}</h2>
                                            <dl>
                                                <dt>Naam</dt>
                                                <dd>{{$subject->name}}</dd>

                                                <dt>Code</dt>
                                                <dd>{{$subject->code}}</dd>

                                                <dt>Aantal studiepunten</dt>
                                                <dd>{{$subject->credit}}</dd>

                                                <dt>Fase</dt>
                                                <dd>{{$subject->phase}}</dd>

                                                <dt>Semester</dt>
                                                <dd>{{$subject->semester}}</dd>

                                                <dt>Periode</dt>
                                                <dd>{{$subject->period}}</dd>

                                            </dl>
                                        @if(Auth::user()->role_id === 1)
                                        <div class="card-body text-right">
                                            <a class="btn btn-outline-warning" href="{{ url('/dashboard/subjects/' . $subject->id . '/edit') }}" role="button"><i class="fa fa-edit (alias)"></i>&nbsp Wijzigen</a>
                                            <a class="btn btn-outline-danger" href="{{ url('/dashboard/subjects/' . $subject->id . '/delete') }}" role="button"><i class="fa fa-trash-o"></i>&nbsp Verwijderen</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


@endsection
