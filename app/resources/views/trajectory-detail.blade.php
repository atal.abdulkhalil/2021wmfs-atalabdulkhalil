@extends('layout')

@section('title', 'Trajecten')

@section('content')

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">{{$trajectory->user->name}} {{$trajectory->user->lastname}}</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Naam</th>
                                        <th scope="col">Code</th>
                                        <th scope="col">Studiepunten</th>
                                        <th scope="col">Fase</th>
                                        <th scope="col">Semester</th>
                                        <th scope="col">Periode</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($subjects as $subject)
                                            <tr>
                                                <td>{{$subject->name}}</td>
                                                <td>{{$subject->code}}</td>
                                                <td>{{$subject->credit}}</td>
                                                <td>{{$subject->phase}}</td>
                                                <td>{{$subject->semester}}</td>
                                                <td>{{$subject->period}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-body text-right">
                            @if($trajectory->accepted == 0)
                                <a class="btn btn-outline-success" href="{{ url('/dashboard/trajectories/' . $trajectory->id . '/accept') }}" role="button"><i class="fa fa-check-square-o"></i>&nbsp Accepteren</a>
                            @else
                                <a class="btn btn-outline-warning" href="{{ url('/dashboard/trajectories/' . $trajectory->id . '/edit') }}" role="button"><i class="fa fa-edit (alias)"></i>&nbsp Wijzigen</a>
                            @endif
                            <a class="btn btn-outline-danger" href="{{ url('/dashboard/trajectories/' . $trajectory->id . '/delete') }}" role="button"><i class="fa fa-trash-o"></i>&nbsp Verwijderen</a>

                        </div>
                        <div class="card-body text-center">
                            <a class="btn btn-outline-secondary" href="{{ url('/dashboard/trajectories') }}" role="button"><i class="fa fa-hand-o-left"></i>&nbsp terug gaan</a>
                        </div>
                    </div>

                </div>

            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


@endsection
