@extends('layout')

@section('title', 'Dashboard')

@section('content')

    <div class="content" style="margin-bottom:20em;">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title mb-3"><div class="location text-sm-center"><i class="fa fa-folder-open"></i></div></strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <h5 class="text-sm-center mt-2 mb-1">Trajecten beheren</h5>

                            </div>
                            <hr>
                            <div class="card-text text-sm-center">
                                <a class="btn btn-outline-info" href="{{url('dashboard/trajectories')}}" role="button"><i class="fa fa-gears (alias)"></i>&nbsp; Beheren</a>
                            </div>
                        </div>
                    </div>
                </div>
                @can('edit-or-delete-subject')
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title mb-3"><div class="location text-sm-center"><i class="fa fa-group (alias)"></i></div></strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <h5 class="text-sm-center mt-2 mb-1">Studenten beheren</h5>

                            </div>
                            <hr>
                            <div class="card-text text-sm-center">
                                <a class="btn btn-outline-info" href="{{url('dashboard/students')}}" role="button"><i class="fa fa-gears (alias)"></i>&nbsp; Beheren</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endcan

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title mb-3"><div class="location text-sm-center"><i class="fa fa-book"></i></div></strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <h5 class="text-sm-center mt-2 mb-1">Vakken beheren</h5>

                            </div>
                            <hr>
                            <div class="card-text text-sm-center">
                                <a class="btn btn-outline-info" href="{{url('dashboard/subjects')}}" role="button"><i class="fa fa-gears (alias)"></i>&nbsp; Beheren</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


@endsection
