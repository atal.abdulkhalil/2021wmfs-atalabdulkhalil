@extends('layout')

@section('title', 'Trajecten')

@section('content')

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Alle gedeelde trajecten</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="bootstrap-data-table" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Naam</th>
                                        <th scope="col">Gemaakt op</th>
                                        <th scope="col">Acties</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($trajectories as $trajectory)
                                            <tr>
                                                <td>{{$trajectory->user->name}} {{$trajectory->user->lastname}}</td>
                                                <td>{{ date('d M y - H\ui', strtotime($trajectory->created_at)) }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a class="btn btn-link font-24 p-0 line-height-1 dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                            <i class="ti-more-alt"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                            <a class="dropdown-item" href="{{ url('/dashboard/trajectories/' . $trajectory->id . '/details') }}"><i class="fa fa-eye"></i> Tonen</a>
{{--                                                            <a class="dropdown-item" href="#"><i class="fa fa-edit (alias)"></i> Wijzigen</a>--}}
                                                            <a class="dropdown-item" href="{{ url('/dashboard/trajectories/' . $trajectory->id . '/delete') }}"><i class="fa fa-trash-o"></i> Verwijderen</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


@endsection
