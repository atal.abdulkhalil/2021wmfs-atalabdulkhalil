@if ($errors->any())
    <div class="vue-lists">
        <div class="alert alert-danger">
            <strong>Something went wrong.</strong>
            <br><br>
            <ul class="vue-list-inner">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif


