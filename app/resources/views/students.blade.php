@extends('layout')

@section('title', 'Studenten')

@section('content')

<div class="content">
    <div class="animated fadeIn">
    <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Alle studenten</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table  table-striped">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>Achternaam</th>
                                <th>Email</th>
                                <th>Acties</th>
                            </tr>
                            </thead>
                            <tbody>
                               @foreach($students as $student)
                                    <tr>
                                        <td>{{$student->name}}</td>
                                        <td>{{$student->lastname}}</td>
                                        <td>{{$student->email}}</td>
                                        <td><a class="btn btn-outline-danger" href="{{ url('/dashboard/students/' . $student->id . '/delete') }}" role="button"><i class="fa fa-trash-o"></i>&nbsp; Verwijderen</a></td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div><!-- .animated -->
</div><!-- .content -->



@endsection
