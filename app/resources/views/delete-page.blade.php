@extends('layout')
@if($student)
    @section('title', 'Studenten')
@else
    @section('title', 'Vakken')
@endif
@section('content')

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    @if($student)
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">{{$item->name}} {{$item->lastname}}</strong>
                            </div>

                            <div class="card-body">
                                <p>Wil je zeker de student <strong class="card-title">{{$item->name}} {{$item->lastname}}</strong>
                                verwijderen?</p>
                            </div>
                            <div class="card-body text-center">

                                <form method="post" action="{{ url('/dashboard/students/' . $item->id . '/delete') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash-o"></i>&nbsp ja</button>
                                </form>

                                <a class="btn btn-outline-secondary mt-3" href="{{ url('/dashboard/students') }}" role="button"><i class="fa fa-hand-o-left"></i>&nbsp terug gaan</a>

                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">{{$item->name}}</strong>
                            </div>

                            <div class="card-body">
                                <p>Wil je zeker het vak <strong class="card-title">{{$item->name}}</strong> met code<strong> {{$item->code}}</strong>
                                    verwijderen?</p>
                            </div>
                            <div class="card-body text-center">

                                <form method="post" action="{{ url('/dashboard/subjects/' . $item->id . '/delete') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash-o"></i>&nbsp ja</button>
                                </form>

                                <a class="btn btn-outline-secondary mt-3" href="{{ url('/dashboard/subjects/' . $item->id . '/details') }}" role="button"><i class="fa fa-hand-o-left"></i>&nbsp terug gaan</a>

                            </div>
                        </div>
                    @endif

                </div>

            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


@endsection
