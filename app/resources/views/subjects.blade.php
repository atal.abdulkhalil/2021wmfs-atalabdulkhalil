@extends('layout')

@section('title', 'Vakken')

@section('content')

    <div class="content">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-8">
                    @include('errors')
                    <div class="card">
                        <div class="card-header">
                            <strong>Vak zoeken</strong>
                        </div>
                        <div class="card-body card-block">
                            <form action="{{ url('/dashboard/subjects') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                @csrf
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="name" class=" form-control-label">Naam</label></div>
                                    <div class="col-12 col-md-9"><input type="text" id="name" name="name" placeholder="Vak naam" class="form-control" value="{{ ($name ?? '') }}"></div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label">Fase</label></div>
                                    <div class="col col-md-9">
                                        <div class="form-check">
                                            <div class="radio">
                                                <label for="phase1" class="form-check-label ">
                                                    <input type="radio" id="phase1" name="phase" value="1" @if(($phase ?? '') === '1') checked="checked" @endif class="form-check-input">Fase 1
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label for="phase2" class="form-check-label ">
                                                    <input type="radio" id="phase2" @if(($phase ?? '') === '2') checked="checked" @endif name="phase" value="2" class="form-check-input">Fase 2
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label for="phase3" class="form-check-label ">
                                                    <input type="radio" id="phase3" name="phase" value="3" @if(($phase ?? '') === '3') checked="checked" @endif class="form-check-input">Fase 3
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="semester" class=" form-control-label">Semester</label></div>
                                    <div class="col-12 col-md-9">
                                        <select name="semester" id="semester" class="form-control">
                                            <option value="0">kies een semester</option>
                                            <option value="1" @if(($semester ?? '') === '1') selected="selected" @endif>semester 1</option>
                                            <option value="2" @if(($semester ?? '') === '2') selected="selected" @endif>semester 2</option>
                                            <option value="1&2" @if(($semester ?? '') === '1&2') selected="selected" @endif>semester 1 en 2</option>
                                        </select>
                                    </div>
                                </div>

                                <input name="moduleAction" type="hidden" value="search"/>
                                <input class="btn btn-outline-success" type="submit" value="Zoeken">
                            </form>
                        </div>
                    </div>

                </div>
                {{--@if(Auth::user()->role_id === 1)--}}
                @can('edit-or-delete-subject')
                <div class="col-md-4 ml-auto mt-auto">
                    <div class="card">
                        <div class="card-header">
                            <strong>Voeg een nieuwe Vak toe </strong>
                        </div>
                        <div class="card-body">
                            <a class="btn btn-outline-success btn-lg btn-block" href="{{ url('/dashboard/subjects/add') }}" role="button"><i class="fa fa-plus-square-o"></i>&nbsp Toevoegen</a>
                        </div>
                    </div><!-- /# card -->
                </div>
                @endcan

                @if(count($subjects)>0)
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Alle vakken</strong>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="bootstrap-data-table" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">Naam</th>
                                            <th scope="col">Code</th>
                                            <th scope="col">Studiepunten</th>
                                            <th scope="col">Acties</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($subjects as $subject)
                                                <tr>
                                                    <td>{{$subject->name}}</td>
                                                    <td>{{$subject->code}}</td>
                                                    <td>{{$subject->credit}}</td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <a class="btn btn-link font-24 p-0 line-height-1 dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                                <i class="ti-more-alt"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                                <a class="dropdown-item" href="{{ url('/dashboard/subjects/' . $subject->id . '/details') }}"><i class="fa fa-eye"></i> Tonen</a>
                                                                @if(Auth::user()->role_id === 1)
                                                                <a class="dropdown-item" href="{{ url('/dashboard/subjects/' . $subject->id . '/edit') }}"><i class="fa fa-edit (alias)"></i> Wijzigen</a>
                                                                <a class="dropdown-item" href="{{ url('/dashboard/subjects/' . $subject->id . '/delete') }}"><i class="fa fa-trash-o"></i> Verwijderen</a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
{{--                                    {{ $subjects->links() }}--}}
                                </div>
                            </div>
                        </div>
                    </div>

                @else
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    <h4 class="text-sm-center mt-2 mb-1">Geen resultaat gevonden.</h4>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>

                @endif
            </div>

        </div><!-- .animated -->
    </div><!-- .content -->


@endsection
