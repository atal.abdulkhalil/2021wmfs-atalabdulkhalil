# 2021WMFS-AtalAbdulkhalil
# Teaser trajectplanner


## Installation instruction

* Install [git](https://git-scm.com/downloads) and [Docker Desktop](https://www.docker.com/products/docker-desktop).
* Start the Docker Desktop application
* Run from your terminal/cmd

```shel
git clone https://git.ikdoeict.be/atal.abdulkhalil/2021wmfs-atalabdulkhalil.git
```

* When Docker is up and running, run from your terminal/cmd
```shel
cd 2021wmfs-atalabdulkhalil
docker-compose up 
```

* When the containers are up and running, run from a new terminal/cmd
```shel
cd 2021wmfs-atalabdulkhalil
docker-compose exec php-web bash
```

* From the Bash terminal in the php-web container, run the following commands:
```shel
composer install
cp .env.example .env
php artisan key:generate
```
* From the Bash terminal in the php-web container, execute the migrate Artisan command:
```shel
php artisan migrate
```
* Then run the following command to seed the database:
```shel
php artisan db:seed --class=trajectplanner
```

* Browse to http://localhost:8080 for server side 
* Browse to http://localhost:4000 for client side
* Stop the environment in your terminal/cmd by pressing `Ctrl+C`
* In order to avoid conflicts with your lab environment, run from your terminal/cmd
```shel
docker-compose down
```

## Routes client side

* /
* /trajecten
* /trajecten/toevoegen
* /trajecten/id
* /inloggen
* /registreren
