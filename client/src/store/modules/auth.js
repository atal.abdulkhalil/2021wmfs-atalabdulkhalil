import myAxios from '../../myaxios_instance/myAxios';

export default {
  namespaced: true,
  state: {
    user: null,
  },
  getters: {
    isLoggedIn: (state) => !!state.user,
    user: (state) => state.user,
  },
  mutations: {
    setUser(state, data) {
      state.user = data;
    },
  },
  actions: {
    async tryLogIn({ commit }) {
      if (document.cookie.indexOf('XSRF-TOKEN') === -1) {
        return;
      }
      const { data } = await myAxios.get('api/user');
      commit('setUser', data);
    },
    async logIn({ commit }, formData) {
      await myAxios.get('sanctum/csrf-cookie');
      // await myAxios.post('sanctum/login')
      const { data } = await myAxios.post('sanctum/login', formData);
      commit('setUser', data);
    },
    async logOut({ commit }) {
      await myAxios.post('sanctum/logout');
      document.cookie = 'XSRF-TOKEN=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC; Secure;';
      commit('setUser', null);
    },
    async register({ commit }, formData) {
      await myAxios.get('sanctum/csrf-cookie');
      await myAxios.post('api/register', formData);
      // await myAxios.post('sanctum/login')
      const { data } = await myAxios.post('sanctum/login', formData);
      commit('setUser', data);
    },
  },
};
