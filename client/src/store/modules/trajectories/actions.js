import myAxios from '../../../myaxios_instance/myAxios';

export default {
  /* eslint-disable */
  async getTrajectories(context) {
    try {
      const response = await myAxios.get('api/trajectories');
      const trajectories = await response.data.data;
      context.commit('setTrajectories', {
        trajectories,
      });
    } catch (error) {
      console.log(error);
    }
  },
  async crTrajectory(context, subjects) {
    await myAxios.post('api/trajectories', { subjects });
  },
  async shareTrajectory(context, id) {
    await myAxios.put(`api/trajectories/${id}`, { shared: 1 });
  },
  async likeTrajectory(context, id) {
    await myAxios.put(`api/trajectories/${id}`, { favorite: 1 });
  },
  async unlikeTrajectory(context, id) {
    await myAxios.put(`api/trajectories/${id}`, { favorite: 0 });
  },
  async dlTrajectory(context, id) {
    await myAxios.delete(`api/trajectories/${id}`);
  },
};
