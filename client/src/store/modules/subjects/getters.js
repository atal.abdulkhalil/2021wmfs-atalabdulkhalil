export default {
  subjects(state) {
    return state.subjects;
  },
  getChosenSubjects(state) {
    return state.chosenSubjects;
  },
};
