import getters from './getters';
import mutations from './mutations';
import actions from './actions';

export default {
  state() {
    return {
      subjects: [],
      chosenSubjects: [],
    };
  },
  getters,
  mutations,
  actions,
};
