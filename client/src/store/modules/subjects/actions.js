import myAxios from '../../../myaxios_instance/myAxios';

export default {
  /* eslint-disable */
  async getSubjects(context) {
    try {
      const response = await myAxios.get('api/subjects');
      const subjects = await response.data.data;
      context.commit('setSubjects', {
        subjects,
      });
    } catch (error) {
      console.log(error);
    }
  },
};
