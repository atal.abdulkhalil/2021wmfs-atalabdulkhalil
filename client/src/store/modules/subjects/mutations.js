export default {
  setSubjects(state, payload) {
    state.subjects = payload.subjects;
  },
  setChosenSubjects(state, subject) {
    // state.chosenSubjects = subject.chosenSubjects;

    // const subject = state.subjects.filter((x) => x.id === parseInt(id, 8));
    state.chosenSubjects.push(subject);
  },
};
