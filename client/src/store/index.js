import { createStore } from 'vuex';
import auth from './modules/auth';
import subjectsModule from './modules/subjects/index';
import trajectoriesModule from './modules/trajectories/index';

export default createStore({
  modules: {
    auth,
    subjects: subjectsModule,
    trajectories: trajectoriesModule,
  },
  namespaced: true,
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {},
});
