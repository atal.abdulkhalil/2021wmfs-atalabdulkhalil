import { createApp } from 'vue';
import VueAxios from 'vue-axios';
import myAxios from './myaxios_instance/myAxios';
import App from './App.vue';
import router from './router';
import store from './store';

createApp(App)
  .use(store)
  .use(router)
  .use(VueAxios, myAxios)
  .mount('#app');
