import axios from 'axios';

const myAxios = axios.create({
  baseURL: 'http://localhost:8080/',
  withCredentials: true,
});

myAxios.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(error),
);
export default myAxios;
