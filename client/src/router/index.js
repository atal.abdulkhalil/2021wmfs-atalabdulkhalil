import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import auth from '../store/modules/auth';
import store from '../store/index';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/trajecten',
    name: 'Trajecten',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/Trajecten.vue'),
    component: () => import('../views/Trajecten.vue'),
    meta: { requiresAuth: true, title: 'trajecten' },
  },
  {
    path: '/trajecten/:id',
    name: 'Detail',
    component: () => import('../views/Detail.vue'),
    meta: { requiresAuth: true, title: 'traject detail' },
  },
  {
    path: '/trajecten/toevoegen',
    name: 'Trajectplanner',
    component: () => import('../views/Trajectplanner.vue'),
    meta: { title: 'traject toevoegen' },
  },
  {
    path: '/inloggen',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta: { requiresUnAuth: true, title: 'inloggen' },
  },
  {
    path: '/registreren',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    meta: { requiresUnAuth: true, title: 'registreren' },
  },
  {
    path: '/:notFound(.*)', redirect: { path: '/' },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

// router.beforeEach((to, from, next) => {
//   if (to.meta.requiresAuth && !auth.state.user) {
//     return next({ name: 'Login' });
//   }
//   if (to.meta.requiresUnAuth && auth.state.user) {
//     return next({ name: 'Home' });
//   }
//   return next();
// });

router.beforeEach((to, from, next) => {
  if (!to.matched.some((record) => record.meta.requiresAuth)) {
    next();
  } else if (auth.state.user) {
    next();
  } else {
    store.dispatch('auth/tryLogIn')
      .then(() => next())
      .catch(() => next({ name: 'Login' }));
  }
});

router.afterEach((from) => {
  document.title = from.meta.title || 'Trajectplanner';
});
export default router;
