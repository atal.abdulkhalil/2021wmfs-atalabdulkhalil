export default [
  {
    id: 1, name: 'C# Object Oriented Programming', code: 'QSD150', credit: 6, phase: 2, semester: '1', period: '1', created_at: '2021-03-29T01:09:31.000000Z',
  }, {
    id: 2, name: 'C# Programming Techniques', code: 'JHG452', credit: 6, phase: 2, semester: '1', period: '2', created_at: '2021-03-29T01:09:31.000000Z',
  }, {
    id: 3, name: 'Ethiek', code: 'HJG120', credit: 3, phase: 2, semester: '1', period: '1&2', created_at: '2021-03-29T01:09:31.000000Z',
  },
];
